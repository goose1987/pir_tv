
#include <TimerOne.h>

 
int IRledPin =  13;    // LED connected to digital pin 13
const int sensorPin = 2; //PIR sensor is attached to digital pin 2

int cnt=0;
volatile int state=LOW;

// Wait for the seonsor to calibrate (20 - 60 seconds according to datasheet)
// 60 Seconds in milliseconds
const unsigned int calibrationTime = 60000;

// The setup() method runs once, when the sketch starts
 
void setup()   {                
  // initialize the IR digital pin as an output:
  pinMode(IRledPin, OUTPUT);      
  pinMode(sensorPin,INPUT);
  
  Serial.begin(9600);
  
    // Blink the LED while calibrating
  Serial.println("calibrating...");
  for (unsigned int i=0; i<calibrationTime; i+=100) {
    
  }
  Serial.println("finish");
  
  attachInterrupt(0,motrip,CHANGE);
}
 
void loop()                     
{
  delay(1000); //do nothing for 1 sec
  if(state==HIGH){
    cnt+=1;
    Serial.println(cnt);
    if(cnt>120){
      state=LOW;
      SendChannelUpCode();
      Serial.println("power down");
      //delay(10000);  // wait twenty seconds (20 seconds * 1000 milliseconds)
    }
  
  }
  
  
}
 
// This procedure sends a 38KHz pulse to the IRledPin 
// for a certain # of microseconds. We'll use this whenever we need to send codes
void pulseIR(long microsecs) {
  // we'll count down from the number of microseconds we are told to wait
 
  cli();  // this turns off any background interrupts
 
  while (microsecs > 0) {
    // 38 kHz is about 13 microseconds high and 13 microseconds low
   digitalWrite(IRledPin, HIGH);  // this takes about 3 microseconds to happen
   delayMicroseconds(10);         // hang out for 10 microseconds
   digitalWrite(IRledPin, LOW);   // this also takes about 3 microseconds
   delayMicroseconds(10);         // hang out for 10 microseconds
 
   // so 26 microseconds altogether
   microsecs -= 26;
  }
 
  sei();  // this turns them back on
}
 
void SendChannelUpCode() {
  // This is the code for the CHANNEL + for the downstairs TV COMCAST
  delayMicroseconds(49452); //Time off (Left Column on serial monitor)
  pulseIR(8900);           //Time on  (Right Column on serial monitor)
  delayMicroseconds(4360);
  pulseIR(600);
  delayMicroseconds(500);
  pulseIR(600);
  delayMicroseconds(500);
  pulseIR(600);
  delayMicroseconds(1580);
  pulseIR(620);
  delayMicroseconds(480);
  pulseIR(620);
  delayMicroseconds(500);
  pulseIR(600);
  delayMicroseconds(500);
  pulseIR(600);
  delayMicroseconds(500);
  pulseIR(600);
  delayMicroseconds(500);
  pulseIR(600);
  delayMicroseconds(1580);
  pulseIR(620);
  delayMicroseconds(1580);
  pulseIR(620);
  delayMicroseconds(480);
  pulseIR(620);
  delayMicroseconds(1580);
  pulseIR(620);
  delayMicroseconds(1580);
  pulseIR(620);
  delayMicroseconds(1580);
  pulseIR(620);
  delayMicroseconds(1580);
  pulseIR(620);
  delayMicroseconds(1580);
  pulseIR(620);
  delayMicroseconds(480);
  pulseIR(620);
  delayMicroseconds(480);
  pulseIR(620);
  delayMicroseconds(480);
  pulseIR(620);
  delayMicroseconds(1580);
  pulseIR(620);
  delayMicroseconds(480);
  pulseIR(620);
  delayMicroseconds(480);
  pulseIR(620);
  delayMicroseconds(480);
  pulseIR(620);
  delayMicroseconds(480);
  pulseIR(620);
  delayMicroseconds(1580);
  pulseIR(620);
  delayMicroseconds(1580);
  pulseIR(620);
  delayMicroseconds(1580);
  pulseIR(600);
  delayMicroseconds(500);
  pulseIR(620);
  delayMicroseconds(1580);
  pulseIR(600);
  delayMicroseconds(1580);
  pulseIR(620);
  delayMicroseconds(1580);
  pulseIR(620);
  delayMicroseconds(1580);
  pulseIR(620);
  delayMicroseconds(39300);
  pulseIR(8900);
  delayMicroseconds(2140);
  pulseIR(620);
  delayMicroseconds(28864);
  pulseIR(8900);
  delayMicroseconds(2160);
  pulseIR(620);
  
}
void motrip()
{
  Serial.println("motion");
  if(state==LOW){
  
    state=!state;
    // Turn the LED on
    
    //delay(1000);
    // Tell the host computer we detected motion
    //Serial.println("motion tripped turn tv on");
 
    //int tripped=1;
    //Serial.println("Sending IR signal");
 
    SendChannelUpCode();
    
    //delay(10000);  // wait twenty seconds (20 seconds * 1000 milliseconds)
  }
  
  cnt=0;


}

